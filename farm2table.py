from bs4 import BeautifulSoup
from collections import namedtuple
from openpyxl import Workbook

from datetime import date
from Table import *
import urllib.request
import re
import pprint
import csv

class Page:

    def __init__(self, url):
        """
        Retrieves and stores the urllib.urlopen object for a given url
        """

        self.link = urllib.request.urlopen(url)

        self.xls_file = ""
        self.csv_file = ""

    def get_tables(self):
        """
        Extracts each table on the page and places it in a dictionary.
        Converts each dictionary to a Table object. Returns a list of
        pointers to the respective Table object(s).
        """

        raw_html = self.link.read()
        soup = BeautifulSoup(raw_html, "html.parser")

        self.csv_file = soup.title.text + "-" + str(date.today()) + '.csv'
        self.xls_file = soup.title.text + "-" + str(date.today()) + '.xlsx'

        tables = soup.find_all("table")

        pp = pprint.PrettyPrinter( indent=4 )

        urlPrefix = "http://my2.siteimprove.com/"

        # have to extract each entry using nested loops
        table_list = []

        test_csv = []

        header_csv = ['SiteImprove','Title','URL']
        test_csv.append(header_csv)

        for table in tables:
            # empty dictionary each time represents our table
            table_dict = {}
            rows = table.find_all("tr")

            # count will be the key for each list of values
            count = 0
            for row in rows:
                value_list = []

                test_csv_row = []

                entries = row.find_all("div", {"class":"page-title"})

                for entry in entries:
                    entry_link = entry.a

                    entry_text = entry.a
                    entry_span = []

                    testString = '=HYPERLINK(\"' + urlPrefix + entry.a.get('href') + '", "Link")'

                    test_csv_row.append(testString)

                    for span in entry_link.find_all('span'):
                        tmpText = span.get_text()
                        tmpText = tmpText.replace('/\xad', '/' )

                        if "www.memphis.edu" in tmpText:
                            tmpText = tmpText.replace('http://www.memphis.edu/','/')
                        else:
                            tmpText = tmpText[0:35]

                        test_csv_row.append(tmpText)

                    test_csv.append(test_csv_row)

                # we don't want empty data packages
                if len(value_list) > 0:
                    table_dict[count] = value_list
                    count += 1

#            print( str(len(test_csv)) )

            wb = Workbook(write_only=True)
            ws = wb.create_sheet()

            for entry in test_csv:
                ws.append(entry)

            wb.save( 'reports/' + self.xls_file )

#            with open(self.csv_file, 'w', newline='') as outf:
#                w = csv.writer(outf, dialect="excel")
#                li = test_csv
#                w.writerows(li)

            table_obj = Table(table_dict)
            table_list.append(table_obj)

        return table_list

    def save_tables(self, tables, ignore_small=False):
        """
        Takes an input a list of table objects and saves each
        table to csv format. If ignore_small is True,
        we ignore any tables with 5 entries or fewer. 
        """

        counter = 1
        for table in tables:
            if ignore_small:
                if table.get_metadata().num_entries > 5:
                    name = "table" + str(counter)
                    table.save_table(name)
                    counter += 1
            else:
                name = "table" + str(counter)
                table.save_table(name)
                counter += 1